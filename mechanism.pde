// FourBar fbar = new FourBar(new PVector(200, 200),
// 													 new PVector(270, 200),
// 													 new float[] { 20, 60, 70 });
FourBar fbar = new FourBar(new Point(200, 200),
													 new Point(213, 213),
													 new Point(250, 250),
													 new Point(270, 200));

ArrayList<Point> newFourBar = new ArrayList<Point>();

public enum State {
	RUNNING,            // r
	EDIT,               // e
	EDIT_COUPLER,       // c
	EDIT_FIXED_FOURBAR, // f
	EDIT_MOBILE_FOURBAR // m
};

State state = State.RUNNING;
ArrayList<Point> points = new ArrayList<Point>();
ArrayList<FourBar> fbars = new ArrayList<FourBar>();

// new triangle with new origin
PVector t0 = new PVector(200, 165);
float step = 0.1;
int trajectorySize = floor(2 * PI / step) + 1;

ArrayList<PVector> trajectoryT1;
ArrayList<PVector> trajectoryT2;

void setup() {
	Utils.processing = this;
	trajectoryT1 = new ArrayList<PVector>();
	trajectoryT2 = new ArrayList<PVector>();
	size(400, 400);
	// fbars.add(fbar);
}


void draw() {
	noFill();

	// frameRate(5);
	background(80);
	for(Point p : points)
	{
		p.draw();
	}
	if (state == State.RUNNING)
	{
		// fbar.step(step);
		for(FourBar fb : fbars)
		{
			fb.step(step);
			fb.draw(true);
		}
	}
	// Triangle t = new Triangle(fbar.points[2],
	// 													t0,
	// 													fbar.points[2].dist(t0),
	// 													33,
	// 													22);

	// coupler from fbar
	// Triangle t2 = new Triangle(fbar.points[1],
	// 													 fbar.points[2],
	// 													 118,
	// 													 fbar.lengths[1],
	// 													 77);
	// t.draw();
	// t2.draw();
	// Utils.drawTrajectory(trajectoryT1, t.p3, trajectorySize, color(0,255,0));
	// Utils.drawTrajectory(trajectoryT2, t2.p3, trajectorySize, color(0,255,0));
	// fbar.draw(true);
}

void keyReleased()
{
	if (state == State.EDIT)
	{
		HandleEdit();
	}
	if (state == State.RUNNING)
	{
		HandleRunning();
	}
}

void HandleRunning()
{
	if (key == 'e')
	{
		state = State.EDIT;
  }
}

void HandleEdit()
{
	switch(key)
	{
	case 'c':
		state = State.EDIT_COUPLER;
		break;
	case 'f':
		state = State.EDIT_FIXED_FOURBAR;
		break;
	case 'm':
		state = State.EDIT_MOBILE_FOURBAR;
		break;
	case 'r':
		state = State.RUNNING;
		break;
	default:
		break;
	}

	System.out.println(state);
}

void mouseMoved()
{
	if (state == State.EDIT
			|| state == State.EDIT_COUPLER
			|| state == State.EDIT_MOBILE_FOURBAR
			|| state == State.EDIT_FIXED_FOURBAR)
	{
		for(Point p : points)
		{
			p.pointMouseMoved();
		}
	}
}

void mouseReleased()
{
	mouseReleasedEdit();
	mouseReleasedFourBar();
}

void mouseReleasedEdit()
{
	if (state == State.EDIT)
	{
		boolean isHoveringPoint = false;
		for(Point p : points)
		{
			isHoveringPoint |= p.hover;
		}

		if (!isHoveringPoint)
		{
			points.add(new Point(mouseX, mouseY));
		} else
		{
			refreshMechanisms();
		}
	}
}

void mouseReleasedFourBar()
{
	if (state == State.EDIT_FIXED_FOURBAR)
	{
		Point currentPoint = null;
		for(Point p : points)
		{
			if (p.hover)
			{
				currentPoint = p;
			}
		}

		if (currentPoint != null)
		{
			newFourBar.add(currentPoint);
			if (newFourBar.size() == 4)
			{
				fbars.add(new FourBar(newFourBar.get(0),
															newFourBar.get(1),
															newFourBar.get(2),
															newFourBar.get(3)));
				newFourBar = new ArrayList<Point>();
				state = State.RUNNING;
			}
		}
	}
}

void mouseDragged()
{
	if (state == State.EDIT)
	for(Point p : points)
	{
		p.pointMouseDragged();
	}
}

void refreshMechanisms()
{
	for(FourBar fb : fbars)
	{
		fb.refresh();
	}
}
