class FourBar {

	public ArrayList<Point> points;
	public ArrayList<PVector> trajectoryA;
	public ArrayList<PVector> trajectoryB;
	public float[] lengths;
	public float[] angles;

	// public FourBar(PVector _origin, float[] _lengths, float[] _angles) {
	// 	points = setPoints(_origin, _lengths, _angles);
	// 	lengths = _lengths;
	// 	angles = _angles;
	// 	trajectoryA = new ArrayList<PVector>();
	// 	trajectoryB = new ArrayList<PVector>();
	// }

	// public FourBar(PVector _activeOrigin, PVector _passiveOrigin, float[] _lengths) {
	// 	lengths = _lengths;
	// 	points = setPoints(_activeOrigin, _passiveOrigin);
	// 	trajectoryA = new ArrayList<PVector>();
	// 	trajectoryB = new ArrayList<PVector>();
	// }

	public FourBar(Point p1, Point p2, Point p3, Point p4)
	{
		points = new ArrayList<Point>();
		points.add(p1);
		points.add(p2);
		points.add(p3);
		points.add(p4);

		refresh();
	}

	public void refresh()
	{
		lengths = new float[] {
			points.get(0).dist(points.get(1)),
			points.get(1).dist(points.get(2)),
			points.get(2).dist(points.get(3))
		};
		angles = new float[] {
			points.get(0).getAngle(points.get(1)),
			points.get(1).getAngle(points.get(2)),
			points.get(2).getAngle(points.get(3))
		};

		trajectoryA = new ArrayList<PVector>();
		trajectoryB = new ArrayList<PVector>();
	}

	// private PVector[] setPoints(PVector origin,
	// 														float[] lengths,
	// 														float[] angles)
	// {
	// 	points = new PVector[4];
	// 	points[0] = origin;

	// 	for(int i = 0; i < lengths.length; i++)
	// 	{
	// 		points[i + 1] = new PVector(points[i].x + lengths[i] * cos(angles[i]),
	// 														points[i].y + lengths[i] * sin(angles[i]));
	// 	}

	// 	return points;
	// }

	// private PVector[] setPoints(PVector activeOrigin,
	// 														PVector passiveOrigin)
	// {
	// 	points = new PVector[4];

	// 	points[0] = activeOrigin;
	// 	points[1] = new PVector();
	// 	points[2] = new PVector();
	// 	points[3] = passiveOrigin;

	// 	angles = new float[3];
	// 	angles[0] = 0;
	// 	step(0);

	// 	return points;
	// }

	public void draw(boolean drawTrajectory)
	{
		for(int i = 0; i < points.size() - 1; i++)
		{
			line(points.get(i).position.x, points.get(i).position.y,
					 points.get(i+1).position.x, points.get(i+1).position.y);
		}

		if (drawTrajectory)
		{
			if (angles[0] < 2 * PI)
			{
				trajectoryA.add(new PVector(points.get(1).position.x, points.get(1).position.y));
				trajectoryB.add(new PVector(points.get(2).position.x, points.get(2).position.y));
			}

			// TODO: move to Point.draw()
			int red = color(255,0,0);
			int blue = color(0,0,255);
			Utils.drawPoint(points.get(0).position, blue);
			Utils.drawPoint(points.get(1).position, red);
			Utils.drawPoint(points.get(2).position, red);
			Utils.drawPoint(points.get(3).position, blue);

			strokeWeight(0);
			stroke(0, 255, 0);
			for (int i = 0; i < trajectoryA.size() -1; i++)
			{
				line(trajectoryA.get(i).x, trajectoryA.get(i).y,
						 trajectoryA.get(i + 1).x, trajectoryA.get(i + 1).y);

			}
			for (int i = 0; i < trajectoryB.size() -1; i++)
			{
				line(trajectoryB.get(i).x, trajectoryB.get(i).y,
						 trajectoryB.get(i + 1).x, trajectoryB.get(i + 1).y);

			}
			stroke(0);
			strokeWeight(1);
		}
	}

	public PVector GetPoint(int index)
	{
		if (index == 0 || index == 3)
		{
			return points.get(index).position;
		}
		int i = index - 1;
		return new PVector(points.get(i).position.x + lengths[i] * cos(angles[i]),
											 points.get(i).position.y + lengths[i] * sin(angles[i]));
	}

	public void step(float angleStep)
	{
		angles[0] = angles[0] + angleStep;

		points.get(1).position.x = points.get(0).position.x + lengths[0] * cos(angles[0]);
		points.get(1).position.y = points.get(0).position.y + lengths[0] * sin(angles[0]);

		updatePositions();
	}

	public void updatePositions()
	{
		Point p1 = points.get(1);
		Point p2 = points.get(3);
		float a = lengths[2];
		float b = points.get(1).dist(points.get(3));
		float c = lengths[1];

		Triangle t = new Triangle(p1, p2, a, b, c);
		angles[1] = t.angle;
		points.get(2).position.x = t.p3.position.x;
		points.get(2).position.y = t.p3.position.y;
	}
}
