public class Triangle
{

	public Point p1;
	public Point p2;
	public Point p3;
	float a;
	float b;
	float c;
	float alpha;
	float omega;
	float angle;
	int shit;

	public Triangle(Point _point1, Point _point2, Point _point3)
	{
		p1 = _point1;
		p2 = _point2;
		p3 = _point3;
	}

	public Triangle(Point _p1, Point _p2, float _a, float _b, float _c)
	{
		p1 = _p1;
		p2 = _p2;
		a = _a;
		b = _b;
		c = _c;

		alpha = getAlpha(a, b, c);

		omega = p1.getAngle(p2);
		angle = omega - alpha;
		p3 = new Point(p1.position.x + c * cos(angle),
									 p1.position.y + c * sin(angle));
	}

	public void draw()
	{
		int constraintColor = color(0, 0, 255);
		Utils.drawPoint(p2.position, constraintColor);
		// line(p1.x, p1.y, p2.x, p2.y);
		line(p2.position.x, p2.position.y, p3.position.x, p3.position.y);
		line(p3.position.x, p3.position.y, p1.position.x, p1.position.y);
	}

	/**
	 * Give a triangle with sides a, b, c and oposing angles
	 * alpha, beta, gamma, get angle alpha from known sides a, b, c
	 */
	public float getAlpha(float a, float b, float c)
	{
		float cosAlpha = (pow(b, 2) +
											pow(c, 2) -
											pow(a, 2)) /
			(2 * b * c);

		return acos(cosAlpha);
	}
}
