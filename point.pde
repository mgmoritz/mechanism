public class Point
{
	public PVector position;
	public PVector size = new PVector(10, 10);
	public boolean hover = false;
	int strokeColor = 0;

	public Point(float x, float y)
	{
		position = new PVector(x, y);
	}

	public Point(float x, float y, int _color)
	{
		position = new PVector(x, y);
		strokeColor = _color;
	}

	public Point(float x, float y, int _color, PVector _size)
	{
		position = new PVector(x, y);
		strokeColor = _color;
		size = _size;
	}

	public Point(float x, float y, PVector _size)
	{
		position = new PVector(x, y);
		size = _size;
	}

	public float dist(Point p2)
	{
		return position.dist(p2.position);
	}

	public float getAngle(Point p2)
	{
		return Utils.getAngle(position, p2.position);
	}

	public void draw()
	{
		drawHover();
		stroke(strokeColor);
		ellipse(position.x, position.y, size.x, size.y);
		stroke(0);
	}

	void drawHover()
	{
		if(hover)
		{
			int c = color(255,0,0,30);
			noStroke();
			fill(c);
			ellipse(position.x, position.y, 3 * size.x, 3 * size.y);
			noFill();
			stroke(0);
		}
	}

	void pointMouseMoved()
	{
		pointMouseHover();
	}

	void pointMouseHover()
	{
		PVector mouse = new PVector(mouseX, mouseY);
		hover = (mouse.dist(position) < 2 * size.x);
	}

	void pointMouseDragged()
	{
		if (hover)
		{
			position.x = mouseX;
			position.y = mouseY;
		}
	}

	public String toString()
	{
		return "(" + position.x + ",  " + position.y + ")";
	}
}
