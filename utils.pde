public static class Utils
{
	public static PApplet processing;
	public static float getAngle(PVector p1, PVector p2)
	{
		float diffY = p1.y - p2.y;
		float diffX = p1.x - p2.x;
		return atan(diffY/diffX);
	}

	public static void drawPoint(PVector p, int c)
	{
		processing.stroke(c);
		processing.ellipse(p.x, p.y, 10, 10);
		processing.stroke(0);
		processing.strokeWeight(1);
	}

	public static void drawTrajectory(ArrayList<PVector> t, PVector p, int size, int c)
	{
		if (t.size() < size)
		{
			t.add(new PVector(p.x, p.y));
		}
		processing.strokeWeight(0);
		processing.stroke(c);
		for (int i = 0; i < t.size() -1; i++)
		{
			processing.line(t.get(i).x, t.get(i).y,
											t.get(i + 1).x, t.get(i + 1).y);

		}

		processing.stroke(0);
		processing.strokeWeight(1);
	}
}
